//
//  ViewController.swift
//  TestScrollView
//
//  Created by dmelnikov on 06.07.2021.
//

import UIKit

enum ScrollViewProcessState {
    case close
    case process(CGFloat)
    case open
}

protocol ScrollActionProcessDelegate: AnyObject {
    func scrollViewActionProcess(_ state: ScrollViewProcessState)
}

class ViewController: UIViewController {
    let headerView: TradingPriceHeaderView = {
        let view = TradingPriceHeaderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        return tableView
    }()
    
    private var headerViewHeightConstraint: NSLayoutConstraint!
    
    var delegate: ScrollActionProcessDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        delegate = headerView
        tableView.delegate = self
        tableView.dataSource = self
        headerViewHeightConstraint = headerView.heightAnchor.constraint(equalToConstant: Constants.headerViewMaxHeight)
        
        view.addSubview(headerView)
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            headerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            headerViewHeightConstraint,
            
            tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            tableView.widthAnchor.constraint(equalTo: view.widthAnchor),
            tableView.topAnchor.constraint(equalTo: headerView.bottomAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
}

//MARK: - UIScrollViewDelegate
extension ViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let newHeaderViewHeight = headerViewHeightConstraint.constant - scrollView.contentOffset.y
        let coefficient = newHeaderViewHeight / Constants.headerViewMaxHeight
        let processFactor = coefficient < 0 ? 0 : coefficient > 1 ? 1 : coefficient

        if newHeaderViewHeight >= Constants.headerViewMaxHeight {
            headerViewHeightConstraint.constant = Constants.headerViewMaxHeight
            delegate?.scrollViewActionProcess(.open)
        } else if newHeaderViewHeight <= Constants.headerViewMinHeight {
            headerViewHeightConstraint.constant = Constants.headerViewMinHeight
            delegate?.scrollViewActionProcess(.close)
        } else {
            headerViewHeightConstraint.constant = newHeaderViewHeight
            delegate?.scrollViewActionProcess(.process(processFactor))
            scrollView.contentOffset.y = 0
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let newHeaderViewHeight = headerViewHeightConstraint.constant - scrollView.contentOffset.y

        if newHeaderViewHeight >= Constants.headerViewMinHeight && newHeaderViewHeight <= Constants.headerViewMaxHeight {
            headerViewHeightConstraint.constant = Constants.headerViewMaxHeight
            delegate?.scrollViewActionProcess(.open)

            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let newHeaderViewHeight = headerViewHeightConstraint.constant - scrollView.contentOffset.y
        
        if newHeaderViewHeight >= Constants.headerViewMinHeight && newHeaderViewHeight <= Constants.headerViewMaxHeight {
            headerViewHeightConstraint.constant = Constants.headerViewMaxHeight
            delegate?.scrollViewActionProcess(.open)
            
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

//MARK: - UITableViewDelegate
extension ViewController: UITableViewDelegate {
}

//MARK: - UITableViewDataSource
extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = "TEST CELL"
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

//MARK: - Constants
private extension ViewController {
    enum Constants {
        static let viewHeight: CGFloat = 500
        static let headerViewMaxHeight: CGFloat = 74
        static let headerViewMinHeight: CGFloat = 40
    }
}
