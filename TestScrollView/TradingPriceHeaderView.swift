//
//  TradingPriceHeaderView.swift
//  TestScrollView
//
//  Created by dmelnikov on 06.07.2021.
//

import UIKit

final class TradingPriceHeaderView: UIView {
    //MARK: - UI components
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Constants.defaultLabelText
        label.textColor = Constants.navigationBarTitleTextColor
        label.font = UIFont.systemFont(ofSize: 34, weight: .bold)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.1
        label.backgroundColor = .red
        
        return label
    }()
    
    private lazy var changeValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = Constants.defaultLabelText
        label.textColor = Constants.navigationBarTitleTextColor
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        
        return label
    }()
    
    private lazy var sellView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.layer.borderWidth = 1 / UIScreen.main.scale
        view.layer.cornerRadius = 8
        view.layer.borderColor = Constants.neutralTextColor.cgColor
        view.clipsToBounds = true
        
        return view
    }()
    
    private lazy var buyView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.layer.borderWidth = 1 / UIScreen.main.scale
        view.layer.cornerRadius = 8
        view.layer.borderColor = Constants.neutralTextColor.cgColor
        view.clipsToBounds = true
        
        return view
    }()
    
    private var changeValueLabelTopAnchorConstraint: NSLayoutConstraint!
    private var changeValueLabelCenterAnchorConstraint: NSLayoutConstraint!
    private var changeValueLabelLeadingAnchorOpenedConstraint: NSLayoutConstraint!
    private var changeValueLabelLeadingAnchorClosedConstraint: NSLayoutConstraint!
    private var changeValueLabelTralingAnchorClosedConstraint: NSLayoutConstraint!
    
    private var priceLabelWidthConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(priceLabel)
        addSubview(changeValueLabel)
        addSubview(sellView)
        addSubview(buyView)
        
        backgroundColor = .green
        
        changeValueLabelTopAnchorConstraint = changeValueLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor)
        changeValueLabelCenterAnchorConstraint = changeValueLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
        changeValueLabelLeadingAnchorOpenedConstraint = changeValueLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16)
        changeValueLabelLeadingAnchorClosedConstraint = changeValueLabel.leadingAnchor.constraint(equalTo: priceLabel.trailingAnchor, constant: 6)
        changeValueLabelTralingAnchorClosedConstraint = changeValueLabel.trailingAnchor.constraint(equalTo: sellView.leadingAnchor, constant: -16)
        
        priceLabelWidthConstraint = priceLabel.trailingAnchor.constraint(equalTo: sellView.leadingAnchor, constant: -16)
        
        changeValueLabelCenterAnchorConstraint.isActive = false
        changeValueLabelLeadingAnchorClosedConstraint.isActive = false
        changeValueLabelTralingAnchorClosedConstraint.isActive = false
        
        NSLayoutConstraint.activate([
            priceLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            priceLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
//            priceLabel.trailingAnchor.constraint(equalTo: sellView.leadingAnchor, constant: -16),
            priceLabelWidthConstraint,
            
            changeValueLabelTopAnchorConstraint,
            changeValueLabelLeadingAnchorOpenedConstraint,
            changeValueLabel.trailingAnchor.constraint(equalTo: sellView.leadingAnchor, constant: -16),
            
            buyView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            buyView.widthAnchor.constraint(equalToConstant: 98),
            buyView.centerYAnchor.constraint(equalTo: centerYAnchor),
            buyView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 11),
            buyView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -11),
            
            sellView.trailingAnchor.constraint(equalTo: buyView.leadingAnchor, constant: -8),
            sellView.widthAnchor.constraint(equalToConstant: 98),
            sellView.centerYAnchor.constraint(equalTo: centerYAnchor),
            sellView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 11),
            sellView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -11),
        ])
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

//MARK: - ScrollActionProcessDelegate
extension TradingPriceHeaderView: ScrollActionProcessDelegate {
    func scrollViewActionProcess(_ state: ScrollViewProcessState) {
        switch state {
        case .close:
//            changeValueLabelTopAnchorConstraint.isActive = false
//            changeValueLabelCenterAnchorConstraint.isActive = true
//            changeValueLabelLeadingAnchorOpenedConstraint.isActive = false
//            changeValueLabelLeadingAnchorClosedConstraint.isActive = true
//            changeValueLabelTralingAnchorClosedConstraint.isActive = true
//            changeValueLabel.isHidden = false
            priceLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
//                NSMutableAttributedString(string: Constants.defaultLabelText, attributes:
//                                    [NSAttributedString.Key.foregroundColor: Constants.navigationBarTitleTextColor,
//                                     NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: .bold)
//                                    ])
//                + NSMutableAttributedString(string: Constants.defaultLabelText, attributes:
//                                    [NSAttributedString.Key.foregroundColor: Constants.positiveTextColor,
//                                     NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .bold)
//                                    ])
        case .open:
//            changeValueLabelTopAnchorConstraint.isActive = true
//            changeValueLabelCenterAnchorConstraint.isActive = false
//            changeValueLabelLeadingAnchorOpenedConstraint.isActive = true
//            changeValueLabelLeadingAnchorClosedConstraint.isActive = false
//            changeValueLabelTralingAnchorClosedConstraint.isActive = false
            priceLabel.font = UIFont.systemFont(ofSize: 34, weight: .bold)
            changeValueLabel.isHidden = false
        case .process(let processFactor):
            print(processFactor)
            priceLabelWidthConstraint.constant = -16 / processFactor
            changeValueLabel.isHidden = true
        }
    }
}

//MARK: - Constants
private extension TradingPriceHeaderView {
    enum Constants {
        static let positiveTextColor: UIColor = .green
        static let negativeTextColor: UIColor = .red
        static let neutralTextColor: UIColor = .gray
        static let listDescriptionTextColor: UIColor = .lightGray
        static let navigationBarTitleTextColor: UIColor = .darkGray
        
        static let defaultLabelText = "123 123,23"
        static let bidPriceTitleText = "Bid"
        static let askPriceTitleText = "Ask"
    }
}
